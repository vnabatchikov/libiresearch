////////////////////////////////////////////////////////////////////////////////
/// DISCLAIMER
///
/// Copyright 2017 ArangoDB GmbH, Cologne, Germany
///
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///     http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.
///
/// Copyright holder is ArangoDB GmbH, Cologne, Germany
///
/// @author Andrey Abramov
/// @author Vasiliy Nabatchikov
////////////////////////////////////////////////////////////////////////////////

#include "tests_shared.hpp"
#include "error/error.hpp"
#include "search/scorers.hpp"
#include "search/sort.hpp"
#include "store/data_output.hpp"

NS_LOCAL

struct bstring_data_output: public irs::data_output {
  irs::bstring out_;
  virtual void close() override {}
  virtual void write_byte(irs::byte_type b) override { write_bytes(&b, 1); }
  virtual void write_bytes(const irs::byte_type* b, size_t size) override {
    out_.append(b, size);
  }
};

NS_END

TEST(sort_tests, order_equal) {
  struct dummy_scorer0: public irs::sort {
    DECLARE_SORT_TYPE() { static irs::sort::type_id type("dummy_scorer0"); return type; }
    static ptr make() { return std::make_shared<dummy_scorer0>(); }
    dummy_scorer0(): irs::sort(dummy_scorer0::type()) { }
    virtual prepared::ptr prepare() const override { return nullptr; }
  };

  struct dummy_scorer1: public irs::sort {
    DECLARE_SORT_TYPE() { static irs::sort::type_id type("dummy_scorer1"); return type; }
    static ptr make() { return std::make_shared<dummy_scorer1>(); }
    dummy_scorer1(): irs::sort(dummy_scorer1::type()) { }
    virtual prepared::ptr prepare() const override { return nullptr; }
  };

  // empty == empty
  {
    irs::order ord0;
    irs::order ord1;
    ASSERT_TRUE(ord0 == ord1);
    ASSERT_FALSE(ord0 != ord1);
  }

  // empty == !empty
  {
    irs::order ord0;
    irs::order ord1;
    ord1.add<dummy_scorer1>(false);
    ASSERT_FALSE(ord0 == ord1);
    ASSERT_TRUE(ord0 != ord1);
  }

  // different sort types
  {
    irs::order ord0;
    irs::order ord1;
    ord0.add<dummy_scorer0>(false);
    ord1.add<dummy_scorer1>(false);
    ASSERT_FALSE(ord0 == ord1);
    ASSERT_TRUE(ord0 != ord1);
  }

  // different order same sort type
  {
    irs::order ord0;
    irs::order ord1;
    ord0.add<dummy_scorer0>(false);
    ord0.add<dummy_scorer1>(false);
    ord1.add<dummy_scorer1>(false);
    ord1.add<dummy_scorer0>(false);
    ASSERT_FALSE(ord0 == ord1);
    ASSERT_TRUE(ord0 != ord1);
  }

  // different number same sorts
  {
    irs::order ord0;
    irs::order ord1;
    ord0.add<dummy_scorer0>(false);
    ord1.add<dummy_scorer0>(false);
    ord1.add<dummy_scorer0>(false);
    ASSERT_FALSE(ord0 == ord1);
    ASSERT_TRUE(ord0 != ord1);
  }

  // different number different sorts
  {
    irs::order ord0;
    irs::order ord1;
    ord0.add<dummy_scorer0>(false);
    ord1.add<dummy_scorer1>(false);
    ord1.add<dummy_scorer1>(false);
    ASSERT_FALSE(ord0 == ord1);
    ASSERT_TRUE(ord0 != ord1);
  }

  // same sorts same types
  {
    irs::order ord0;
    irs::order ord1;
    ord0.add<dummy_scorer0>(false);
    ord0.add<dummy_scorer1>(false);
    ord1.add<dummy_scorer0>(false);
    ord1.add<dummy_scorer1>(false);
    ASSERT_TRUE(ord0 == ord1);
    ASSERT_FALSE(ord0 != ord1);
  }
}

TEST(sort_tests, test_collector_serialization) {
  struct field_collector: public irs::sort::field_collector {
    virtual void collect(const irs::sub_reader& segment, const irs::term_reader& field) override {}
    virtual void collect(const irs::bytes_ref& in) override { values().emplace_back(in); }
    static size_t& value_offset() { static size_t offset = 0; return offset; }
    static std::vector<irs::bstring>& values() { static std::vector<irs::bstring> values; return values; }
    virtual void write(irs::data_output& out) const override {
      auto& value = values()[value_offset()++];
      out.write_bytes(&value[0], value.size());
    }
  };
  struct term_collector: public irs::sort::term_collector {
    virtual void collect(const irs::sub_reader& segment, const irs::term_reader& field, const irs::attribute_view& term_attrs) override {}
    virtual void collect(const irs::bytes_ref& in) override { values().emplace_back(in); }
    static size_t& value_offset() { static size_t offset = 0; return offset; }
    static std::vector<irs::bstring>& values() { static std::vector<irs::bstring> values; return values; }
    virtual void write(irs::data_output& out) const override {
      auto& value = values()[value_offset()++];
      out.write_bytes(&value[0], value.size());
    }
  };
  struct prepared: public irs::sort::prepared {
    bool null_field_collector{false};
    bool null_term_collector{false};
    virtual void add(irs::byte_type* dst, const irs::byte_type* src) const override {}
    virtual void collect(irs::attribute_store& filter_attrs, const irs::index_reader& index, const irs::sort::field_collector* field, const irs::sort::term_collector* term) const override {}
    virtual const irs::flags& features() const override { return irs::flags::empty_instance(); }
    virtual bool less(const irs::byte_type* lhs, const irs::byte_type* rhs) const override { return false; }
    virtual irs::sort::field_collector::ptr prepare_field_collector() const override { return null_field_collector ? nullptr : irs::memory::make_unique<field_collector>(); }
    virtual irs::sort::scorer::ptr prepare_scorer( const irs::sub_reader& segment, const irs::term_reader& field, const irs::attribute_store& query_attrs, const irs::attribute_view& doc_attrs) const override { return nullptr; }
    virtual irs::sort::term_collector::ptr prepare_term_collector() const override { return null_term_collector ? nullptr : irs::memory::make_unique<term_collector>(); }
    virtual void prepare_score(irs::byte_type* score) const override {}
    virtual size_t size() const override { return 0; }
  };

  // serialized single sort
  {
    std::vector<irs::order::prepared::prepared_sort> buckets;
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    std::vector<irs::bstring> expected_fields = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("abcd")),
    };
    std::vector<irs::bstring> expected_terms = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("efgh")),
    };

    field_collector::values() = expected_fields;
    term_collector::values() = expected_terms;
    irs::order::prepared::collectors collectors0(buckets, 1);
    bstring_data_output out;
    collectors0.write(out);

    field_collector::value_offset() = 0;
    term_collector::value_offset() = 0;
    field_collector::values().clear();
    term_collector::values().clear();
    irs::order::prepared::collectors collectors1(buckets, 1);
    collectors1.collect(out.out_);

    ASSERT_EQ(expected_fields.size(), field_collector::values().size());
    ASSERT_EQ(expected_terms.size(), term_collector::values().size());

    for (size_t i = 0, count = expected_fields.size(); i < count; ++i) {
      ASSERT_EQ(expected_fields[i], field_collector::values()[i]);
    }

    for (size_t i = 0, count = expected_terms.size(); i < count; ++i) {
      ASSERT_EQ(expected_terms[i], term_collector::values()[i]);
    }
  }

  // serialized multiple sorts
  {
    std::vector<irs::order::prepared::prepared_sort> buckets;
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    std::vector<irs::bstring> expected_fields = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("abc")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("defg")),
    };
    std::vector<irs::bstring> expected_terms = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("hijkl")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("mnopqr")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("stuvwxy")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("42")),
    };

    field_collector::value_offset() = 0;
    term_collector::value_offset() = 0;
    field_collector::values() = expected_fields;
    term_collector::values() = expected_terms;
    irs::order::prepared::collectors collectors0(buckets, 2);
    bstring_data_output out;
    collectors0.write(out);

    field_collector::values().clear();
    term_collector::values().clear();
    irs::order::prepared::collectors collectors1(buckets, 2);
    collectors1.collect(out.out_);

    ASSERT_EQ(expected_fields.size(), field_collector::values().size());
    ASSERT_EQ(expected_terms.size(), term_collector::values().size());

    for (size_t i = 0, count = expected_fields.size(); i < count; ++i) {
      ASSERT_EQ(expected_fields[i], field_collector::values()[i]);
    }

    for (size_t i = 0, count = expected_terms.size(); i < count; ++i) {
      ASSERT_EQ(expected_terms[i], term_collector::values()[i]);
    }
  }

  // serialized null field_collector and non-empty init
  {
    std::vector<irs::order::prepared::prepared_sort> buckets;
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    std::vector<irs::bstring> expected_fields = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("abc")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("defg")),
    };
    std::vector<irs::bstring> expected_terms = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("hijkl")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("mnopqr")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("stuvwxy")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("42")),
    };

    field_collector::value_offset() = 0;
    term_collector::value_offset() = 0;
    field_collector::values() = expected_fields;
    term_collector::values() = expected_terms;
    irs::order::prepared::collectors collectors0(buckets, 2);
    bstring_data_output out;
    collectors0.write(out);

    field_collector::values().clear();
    term_collector::values().clear();
    static_cast<prepared*>(buckets.back().bucket.get())->null_field_collector = true; // set null field_collector
    irs::order::prepared::collectors collectors1(buckets, 2);
    ASSERT_THROW(collectors1.collect(out.out_), irs::io_error);
  }

  // serialized null term_collector and non-empty init
  {
    std::vector<irs::order::prepared::prepared_sort> buckets;
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    std::vector<irs::bstring> expected_fields = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("abc")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("defg")),
    };
    std::vector<irs::bstring> expected_terms = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("hijkl")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("mnopqr")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("stuvwxy")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("42")),
    };

    field_collector::value_offset() = 0;
    term_collector::value_offset() = 0;
    field_collector::values() = expected_fields;
    term_collector::values() = expected_terms;
    irs::order::prepared::collectors collectors0(buckets, 2);
    bstring_data_output out;
    collectors0.write(out);

    field_collector::values().clear();
    term_collector::values().clear();
    static_cast<prepared*>(buckets.back().bucket.get())->null_term_collector = true; // set null term_collector
    irs::order::prepared::collectors collectors1(buckets, 2);
    ASSERT_THROW(collectors1.collect(out.out_), irs::io_error);
  }

  // serialized too short (field_collector)
  {
    std::vector<irs::order::prepared::prepared_sort> buckets;
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    std::vector<irs::bstring> expected_fields = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("abc")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("defg")),
    };
    std::vector<irs::bstring> expected_terms = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("hijkl")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("mnopqr")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("stuvwxy")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("42")),
    };

    field_collector::value_offset() = 0;
    term_collector::value_offset() = 0;
    field_collector::values() = expected_fields;
    term_collector::values() = expected_terms;
    irs::order::prepared::collectors collectors0(buckets, 2);
    bstring_data_output out;
    collectors0.write(out);

    field_collector::values().clear();
    term_collector::values().clear();
    irs::order::prepared::collectors collectors1(buckets, 2);
    ASSERT_THROW(collectors1.collect(irs::bytes_ref(&out.out_[0], 2)), irs::io_error);
  }

  // serialized too short (term_collector)
  {
    std::vector<irs::order::prepared::prepared_sort> buckets;
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    std::vector<irs::bstring> expected_fields = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("abc")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("defg")),
    };
    std::vector<irs::bstring> expected_terms = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("hijkl")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("mnopqr")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("stuvwxy")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("42")),
    };

    field_collector::value_offset() = 0;
    term_collector::value_offset() = 0;
    field_collector::values() = expected_fields;
    term_collector::values() = expected_terms;
    irs::order::prepared::collectors collectors0(buckets, 2);
    bstring_data_output out;
    collectors0.write(out);

    field_collector::values().clear();
    term_collector::values().clear();
    irs::order::prepared::collectors collectors1(buckets, 2);
    ASSERT_THROW(collectors1.collect(irs::bytes_ref(&out.out_[0], out.out_.size() - 2)), irs::io_error);
  }

  // serialized too long
  {
    std::vector<irs::order::prepared::prepared_sort> buckets;
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    buckets.emplace_back(irs::memory::make_unique<prepared>(), false);
    std::vector<irs::bstring> expected_fields = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("abc")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("defg")),
    };
    std::vector<irs::bstring> expected_terms = {
      irs::ref_cast<irs::byte_type>(irs::string_ref("hijkl")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("mnopqr")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("stuvwxy")),
      irs::ref_cast<irs::byte_type>(irs::string_ref("42")),
    };

    field_collector::value_offset() = 0;
    term_collector::value_offset() = 0;
    field_collector::values() = expected_fields;
    term_collector::values() = expected_terms;
    irs::order::prepared::collectors collectors0(buckets, 2);
    bstring_data_output out;
    collectors0.write(out);

    field_collector::values().clear();
    term_collector::values().clear();
    out.out_.append(irs::ref_cast<irs::byte_type>(irs::string_ref("12345")));
    irs::order::prepared::collectors collectors1(buckets, 2);
    ASSERT_THROW(collectors1.collect(out.out_), irs::io_error);
  }
}

// -----------------------------------------------------------------------------
// --SECTION--                                                       END-OF-FILE
// -----------------------------------------------------------------------------